// ==UserScript==
// @name        Humble Book Bundle Helper
// @description Search the url of the books and composes a command to download with wget from a CLI
// @include     https://www.humblebundle.com/downloads?key=*
// @version     1.2.1
// @grant       none
// @downloadURL https://gitlab.com/ariacorrente/user-scripts/raw/master/humble-book-bundle-helper.user.js
// ==/UserScript==

(function() {

"use strict";

const debugEnabled = true;

let clog = getClog("HBBH", debugEnabled),
    formats = [],
    checkboxes = new Map(),
    menuElement,
    summaryElement,
    dialogWrapper,
    outWrapper,
    buttonCloseDialog,
    commandString,
    filesToDownload = 0,
    filesFound = 0,
    cssBody = `
.hbbh-menu-wrapper {
  position: fixed;
  top: 0;
  right: 0;
  padding: 1em;
  background-color: #e0e0e0;
  z-index: 1000;
}

.hbbh-menu-wrapper>div{
  font-size: 24px;
  text-align: center;
  border: 1px solid;
  padding: 8px;
  margin: 4px;
  cursor: pointer;
}

.hdb-button:hover {
  background-color: gray;
}


.hbbh-dialog {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 9999;
  display: none;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.75);
}

.hbbh-window {
  position: relative;
  padding: 1em;
  background-color: #e0e0e0;
  width: 800px;
  border: 1px solid black;
}

.terminal {
  padding: 1em;
  border-style: inset;
  border-width: 1px;
  background-color: black;
  color: white;
  overflow: auto;
}

.hbbh-visible {
    display: flex;
}

.hbbh-button-close-dialog {
  line-height: 0.75em;
  text-align: center;
  width: 1em;
  height: 1em;
  position: absolute;
  top: 0.5em;
  right: 0.5em;
  cursor: pointer;
  border: 1px solid black;
  font-size: 2em;
}

.hbbh-button-close-dialog:hover {
  background-color: gray;
}
`;

/*
 * Returns the custom console log function.
 */
function getClog(channelName, debugEnabled = false) {
  let clog;

  if (debugEnabled) {
    clog = function customConsoleLog(...args) {
      let prefix;
      if (channelName !== undefined) {
        prefix = `${channelName}|`;
      } else {
        prefix = 'clog|';
      }
      console.log(prefix, ...args);
    };
  } else {
    clog = function nop() {};
  }
  return clog;
}

/*
 * Creates an HTML element reading the recipe passed under the form of a
 * javascript object.
 */
function buildDom(data) {
  let el;
  let children;

  // Check the root element
  if (!data) {
    // Empty data?
    throw (new Error('Empty data'));
  } else if (data.tag) {
    // Tag found, this is a typical HTML tag
    el = document.createElement(data.tag);
  } else if (data.text) {
    // No tag but a "text" means this is a text-node
    el = document.createTextNode(data.text);
    // No children for text nodes, return now
    return el;
  } else {
    // No tag or a text-field, invalid object
    throw (new Error('Data missing tag or text'));
  }

  // Iterate all the object keys for attributes and children
  const names = Object.keys(data);
  for (let i = 0; i < names.length; i += 1) {
    const name = names[i];
    if (name === 'tag') {
      // Nothing to do, already created the root element
    } else if (name === 'text') {
      // Shortcut for textNodes instead of using a child they are identified
      // by the "text" object name
      const textNode = document.createTextNode(data.text);
      el.appendChild(textNode);
    } else if (name.startsWith('event')) {
      const eventName = name.substring(5).toLowerCase();
      el.addEventListener(eventName, data[name]);
    } else if (name === 'children') {
      ({ children } = data);
      if (Array.isArray(children)) {
        // If multiple children then it is an array and i must iterate
        children.forEach((item) => {
          // Build recursively all children
          if (item) {
            el.appendChild(buildDom(item));
          }
        });
      } else if (children) {
        // Only one child, call recursively
        el.appendChild(buildDom(children));
      }
    } else {
      // Add all other attributes
      el.setAttribute(name, data[name]);
    }
  }
  return el;
}

/*
 * Add a style block to the HTML with the CSS used by the script dialogs.
 */
function createStyle() {
  let styleRecipe = {
    tag: "style",
    text: cssBody
  };
  let styleElement = buildDom( styleRecipe );
  let bodyElement = document.querySelector( "body" );
  if ( bodyElement ) {
    bodyElement.appendChild( styleElement );
  }
}

/*
 * Adds a checkbox inside the main menu allowing the user to choose what format
 * to include in the download command.
 */
function addCheckbox(format) {
  let recipe = {
    tag: "div",
    children: [
      {
        tag: "input",
        type: "checkbox",
        id: "hbbh-" + format,
        name: format,
        value: format,
        checked: true
      },
      {
        tag: "label",
        text: format,
        for: "hbbh-" + format
      }
    ]
  };
  let checkboxWrapper = buildDom(recipe);
  let checkboxElement = checkboxWrapper.querySelector("#hbbh-" + format)
  let fieldset = menuElement.querySelector("fieldset");
  fieldset.appendChild(checkboxWrapper);
  checkboxes.set(format, checkboxElement);
}

/*
 * Creates the main menu on the top right of the screen.
 */
function createMenu() {
  let checkboxes = {
    tag: "fieldset",
    children: [
      {
        tag: "div",
        text: "You first need to 'Search formats'."
      }
    ]
  };
  let menuRecipe = {
    tag: "div",
    class: "hbbh-menu-wrapper",
    children: [
      {
        tag: "h2",
        text: "Humble Book Bundle Helper"
      },
      {
        tag: "div",
        text: "Search formats",
        class: "hbbh-button-search-formats hdb-button"
      },
      {
        tag: "p",
        text: "Select the file formats to download:"
      },
      checkboxes,
      {
        tag: "div",
        text: "Search files",
        class: "hbbh-button-scan hdb-button"
      }
    ]
  }

  menuElement = buildDom( menuRecipe );
  let bodyElement = document.querySelector( "body" );
  if ( bodyElement ) {
    bodyElement.appendChild( menuElement );
  }

  let buttonSearch = menuElement.querySelector(".hbbh-button-search-formats");
  if (buttonSearch) {
    buttonSearch.addEventListener("click", scanFileFormats);
  }

  let buttonScan = menuElement.querySelector( ".hbbh-button-scan" );
  if ( buttonScan ) {
    buttonScan.addEventListener( 'click', scanPage );
  }
}

/*
 * Creates the HTML of the output dialog.
 */
function createDialog() {
  let dialogRecipe = {
    tag: "div",
    class: "hbbh-dialog",
    children: {
      tag: "div",
      class: "hbbh-window",
      children: [
        {
          tag: "h2",
          text: "Scan completed"
        },
        {
          tag: "div",
          text: "×",
          class: "hbbh-button-close-dialog"
        },
         {
          tag: "div",
          id: "hbbh-summary"
        },
        {
          tag: "div",
          text: "Execute the following 'wget' command in a terminal to download all the files in the selected formats:"
        },
        {
          tag: "pre",
          class: "terminal",
          id: "hbbh-out-wrapper"
        },
        {
          tag: "div",
          text: "Then fix the names of the files with a command like:"
        },
        {
          tag: "pre",
          text: "rename 's/\.epub.*/\.epub/' ./*",
          class: "terminal"
        },
        {
          tag: "div",
          text: "Programs required: 'wget' and 'rename' (in Arch linux it is called 'perl-rename')."
        }
      ]
    }
  };
  dialogWrapper = buildDom( dialogRecipe );
  let bodyElement = document.querySelector( "body" );
  if ( bodyElement ) {
    bodyElement.appendChild( dialogWrapper );
  }

  outWrapper = dialogWrapper.querySelector( "#hbbh-out-wrapper" );
  buttonCloseDialog = dialogWrapper.querySelector( ".hbbh-button-close-dialog" );
  summaryElement = dialogWrapper.querySelector("#hbbh-summary");
  if ( buttonCloseDialog ) {
    buttonCloseDialog.addEventListener( "click", toggleDialogVisibility );
  }
}

/*
 * Toggles the visibility of the output dialog.
 */
function toggleDialogVisibility() {
  dialogWrapper.classList.toggle( "hbbh-visible" );
}

/*
 * Makes the output dialog visible.
 */
function showDialog() {
  if( !( dialogWrapper.classList.contains( "hbbh-visible" ) ) ) {
    dialogWrapper.classList.add( "hbbh-visible" );
  }
}

function getSummary() {
  let formatsEnabled = 0;
  let rowCount = document.querySelectorAll(".js-download-rows .row").length;

  checkboxes.forEach(
    (checkbox) => {
      if (checkbox.checked) {
        formatsEnabled = formatsEnabled + 1;
      }
    }
  );

  let msg = "Found " + filesFound + " files for the " + rowCount + " products.";
  msg = msg + " Will be downloaded " + filesToDownload + " files in the " + formatsEnabled + " selected formats.";
  return msg;
}

/*
 * Callback bound to the "Scan" button.
 * Will display a modal dialog containing the instructions to mass-download
 * the files with the formats checked in the menu.
 */
function scanPage() {
  filesToDownload = 0,
  filesFound = 0,
  commandString = "wget -c";
  let anchors = document.querySelectorAll( ".js-start-download a" );
  let textNode;
  filesFound = anchors.length;
  anchors.forEach( composeCommand );

  if ( commandString != "wget -c" ) {
    textNode = document.createTextNode( commandString );
  } else {
    let message = "Download links not found. The script may be broken or you " +
      "need to check at least a format in the form in the top right corner " +
      "of the page.";
    clog( message );
    textNode = document.createTextNode( message );
  }

  let previousText =  outWrapper.firstChild;
  if ( previousText ) {
    outWrapper.removeChild( previousText );
  }
  outWrapper.appendChild( textNode );
  summaryElement.textContent = getSummary();
  showDialog();
}

/*
 * Concatenate the URLs to compose the CLI command.
 */
function composeCommand( anchor ) {
  let href = anchor.href,
      anchorFormat = anchor.text.trim(),
      outText = "",
      checkbox;

  formats.forEach(
    (format) => {
      checkbox = checkboxes.get(format);
      if (format == anchorFormat && checkbox.checked) {
        clog(format + ": " + href);
        outText = outText + " \"" + href + "\"";
        filesToDownload = filesToDownload + 1;
      }
    }
  );

  commandString = commandString + outText;
}

/*
 * Scan for file formats available to download.
 */
function scanFileFormats() {
  // Remove old checkboxes
  let fieldset = menuElement.querySelector("fieldset");
  while (fieldset.firstChild) {
    fieldset.firstChild.remove()
	}

  let anchors = document.querySelectorAll(".js-start-download a");
  anchors.forEach(
    (anchor) => {
      let format = anchor.text.trim();
      if (! formats.includes(format) ) {
         formats.push(format);
      }
    }
  );
  formats.forEach(addCheckbox);
}

function main() {
  clog( "Start" );

  createStyle();
  createMenu();
  createDialog();

  clog( "End" );
}

main();

})();
