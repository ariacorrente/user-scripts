// ==UserScript==
// @name        MV Unlocker
// @description Remove paywall from Messaggero Veneto
// @namespace   ariacorrente
// @match       *://messaggeroveneto.gelocal.it/*
// @version     1.0.5
// @grant       none
// @downloadURL https://gitlab.com/ariacorrente/user-scripts/raw/master/mv-unlocker.user.js
// ==/UserScript==

(function() {

const debugEnabled = false;
let clog,
    articleFound = false;

function initClog() {
  if( debugEnabled ) {
    clog = function( msg ) {
      console.log( "MVU| " + msg );
    };
  } else {
    clog = function(){};
  }
}

function fixPaywall() {
  let article = document.querySelector( "#article-body" );
  if( article !== null ) {
    articleFound = true;

    // Try to fix now
    resetStyle( article );
 
    // And keep watching for changes of the style
    let observer = new MutationObserver( function( mutations ) {
      clog( "Style change detected" );
      resetStyle( article );
    } );
    observer.observe( article, { attributes : true, attributeFilter : ['style'] } );
  } else {
    clog( "Article not found" );
  }
}

function resetStyle( article ) {
  // Let the article grow to all hight and be visible
  if( article.style["max-height"] !== "" ) {
    article.style["max-height"] = "";
    clog( "Max height fixed" );
  } else {
    clog( "Max height is fine" );
  }

  // Avoid overlap of the "before" pseudo-element of the ".paywall-adagio"
  // with the tail of the article
  if( article.style["margin-top"] != "300px" ) {
    article.style["margin-bottom"] = "300px";
    clog( "Adagio gradient overlap" );
  } else {
    clog( "Adagio gradient overlap is fine" );
  }
}

function main() {
  initClog();
  clog( "'MV Unlock' script loaded");
  if( articleFound ) {
    clog( "Article already found" );
  } else {
    clog( "article needs work" );
    fixPaywall();
  }
}

main();

})();
