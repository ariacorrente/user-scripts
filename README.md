# A collection of user scripts

A collection of scripts in Javascript to be used with browser addons as
Greasemonkey & Co. to enhance some web pages.

Scripts available:

- **humble-book-bundle-helper.js**: starting from the download page for a bought
    Huble Book Bunde, the scripts generates a CLI command to mass download the
    files with `wget`.

- **mv-unlocker.js**: fix MessaggeroVeneto
