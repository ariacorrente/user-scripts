// ==UserScript==
// @name         Reddit ager
// @namespace    https://gitlab.com/ariacorrente/user-scripts
// @version      1.0.0
// @description  Redirects new reddit.com to old.reddit.com subdomain
// @match        *://www.reddit.com/*
// @grant        none
// @run-at       document-start
// @downloadURL  https://gitlab.com/ariacorrente/user-scripts/-/raw/master/reddit-ager.user.js
// ==/UserScript==

(function() {

"use strict"

function translateToOldVersion(url){
  let parts = url.split('reddit.com');
  let path =  parts[parts.length - 1];
  // If split fails will return the full original string
  if (path !== url) {
      return 'https://old.reddit.com' + path;
  } else {
    return url;
  }
}

let newUrl = translateToOldVersion(window.location.href);
window.location.assign(newUrl);

})();
